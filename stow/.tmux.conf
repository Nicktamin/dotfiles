# remap prefix from 'C-b' to 'C-a'
#unbind C-b
#set-option -g prefix C-a
#bind-key C-a send-prefix

# Set full color
set-option -g default-terminal "screen-256color"
set-option -g terminal-overrides ',xterm-256color:RGB'

# split panes using "h" and "v"
bind v split-window -h
bind h split-window -v
unbind '"'
unbind %

# reload config file (change file location to your the tmux.conf you want to use)
bind r source-file ~/.tmux.conf

# switch panes using Alt-arrow without prefix
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# Enable mouse mode (tmux 2.1 and above)
#set -g mouse on

# don't rename windows automatically
set-option -g allow-rename off

# scrollback size
set -g history-limit 1000000
set -g history-file ~/.tmux_history

# set first window to index 1 (not 0) to map more to the keyboard layout
set -g base-index 1
set -g pane-base-index 1

# renumber all windows when any window is closed
set -g renumber-windows on

# move statusbar on top
#set -g status-position top

# pass through xterm keys
set -g xterm-keys on

# use system clipboard
set -g set-clipboard on

# Keep your finger on ctrl, or don't, same result
bind-key C-d detach-client
bind-key C-p paste-buffer

# Redraw the client (if interrupted by wall, etc)
bind R refresh-client

# Show tmux positions in titles
set -g set-titles on

# THEME
set -g @catppuccin_flavour 'frappe'
set -g @catppuccin_window_default_fill "number"
set -g @catppuccin_window_default_text "#W"
set -g @catppuccin_window_current_fill "number"
set -g @catppuccin_window_current_text "#W"
set -g @catppuccin_window_left_separator ""
set -g @catppuccin_window_right_separator " "
set -g @catppuccin_window_middle_separator " █"
set -g @catppuccin_window_number_position "right"
set -g @catppuccin_window_default_fill "number"
set -g @catppuccin_window_default_text "#W"
set -g @catppuccin_window_current_fill "number"
set -g @catppuccin_window_current_text "#W#{?window_zoomed_flag,(),}"
set -g @catppuccin_status_modules_right "host date_time"
set -g @catppuccin_status_modules_left "session"
set -g @catppuccin_status_left_separator  " "
set -g @catppuccin_status_right_separator " "
set -g @catppuccin_status_right_separator_inverse "no"
set -g @catppuccin_status_fill "icon"
set -g @catppuccin_status_connect_separator "no"
set -g @catppuccin_directory_text "#{b:pane_current_path}"
set -g @catppuccin_date_time_text "%H:%M"

# TPM plugin manager
# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'sainnhe/tmux-fzf'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-prefix-highlight'
set -g @plugin 'catppuccin/tmux'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
