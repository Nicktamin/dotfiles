# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

######
# ~~ Zsh user configuration
###

######
# Dependency check
###

# set dependencies
PKG_DEP_RPM=(fzf bat fd-find tmux eza trash-cli)
PKG_DEP_DPKG=(fzf bat fd-find tmux exa trash-cli)
PKG_ALL=()

## check and install dependencies
# Fedora
if [ "$(command -v rpm)" ]; then
    # check packages
    for PKG in ${PKG_DEP_RPM=[@]}; do
        if ( ! rpm -q --quiet $PKG ); then
            PKG_ALL+=($PKG)
        fi
    done
    # print packages, ask for install
    if [ ! "${#PKG_ALL[@]}" -eq 0 ]; then
        echo "Missing packages: ${PKG_ALL[@]}"
        echo -n "Install them? (y/n) " ; read -r ANSWER
        case $ANSWER in; [YyJj]* ) sudo dnf install ${PKG_ALL[@]} -yq;; esac
    fi
# Ubuntu/Debian
elif [ "$(command -v dpkg)" ]; then
    # check packages
    for PKG in ${PKG_DEP_DPKG[@]}; do
        if ( ! $(dpkg-query -W -f='${Status}\n' $PKG 2>/dev/null | grep -q "install ok installed") ); then
            PKG_ALL+=($PKG)
        fi
    done
    # print packages, ask for install
    if [ ! "${#PKG_ALL[@]}" -eq 0 ]; then
        echo "Missing packages: ${PKG_ALL[@]}"
        echo -n "Install them? (y/n) " ; read -r ANSWER
        case $ANSWER in; [YyJj]* ) sudo apt-get -qqy install ${PKG_ALL[@]};; esac
    fi
fi

## set variables depending on the OS
BATCMD=""
if [ "$(command -v rpm)" ]; then
    BATCMD="bat"
    FDCMD="fd"
elif [ "$(command -v dpkg)" ]; then
    BATCMD="batcat"
    FDCMD="fdfind"
fi

## check github dependencies
# Antigen
ANTIDIR="$HOME/.config/antigen"
if [ ! -f "$ANTIDIR/antigen.zsh" ]; then
    git clone https://github.com/zsh-users/antigen.git "$ANTIDIR"
fi

# tmux-plugin-manager
TPMDIR="$HOME/.tmux/plugins/tpm"
if [ ! -f "$TPMDIR/tpm" ]; then
    git clone https://github.com/tmux-plugins/tpm "$TPMDIR"
fi

######
# Defaults
###
# Export path
export PATH="$PATH:/home/nick/.local/bin"

# Force colour
export TERM=xterm-256color

# Default editor
export EDITOR='vim'

# Language
export LANG='C.UTF-8'
export LC_ALL='C.UTF-8'

# History
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=1000000
setopt inc_append_history
setopt share_history

# Options
setopt autocd nomatch notify
unsetopt beep extendedglob
#bindkey -v # vim keybinds

# Catpuccin syntax highlighting theme
source ~/.zsh/catppuccin_frappe-zsh-syntax-highlighting.zsh

# Keep ranger from loading the default config\
RANGER_LOAD_DEFAULT_RC=false

######
# Autocomplete
###

# Enable the completion menu
zstyle ':completion:*' menu select

# Match completion menu colors with the LS_COLORS variable
[[ -n $LS_COLORS ]] && zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# Ignore cases if there are no uppercase letters
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'

# Attempt to find new commands to complete
zstyle ':completion:*' rehash true

######
# Antigen
###

# load antigen
source "$ANTIDIR/antigen.zsh"

# oh-my-zsh
#antigen use oh-my-zsh

# oh-my-zsh's plugins
antigen bundle fzf
antigen bundle colored-man-pages
antigen bundle command-not-found

# plugins from github
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-syntax-highlighting

# themes from github
antigen theme romkatv/powerlevel10k

# apply changes
antigen apply

######
# Fuzzyfind
###

export FZF_DEFAULT_OPTS="--no-mouse --height 50% -1 --reverse --inline-info"
export FZF_DEFAULT_COMMAND="$FDCMD --follow --mount --hidden --type f --type l --exclude .git --strip-cwd-prefix"
export FZF_CTRL_T_OPTS="$FZF_DEFAULT_OPTS -- --preview '$BATCMD --style=numbers --color=always --line-range :500 {}'"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="$FDCMD --follow --mount --type d"

# Catppuccin Theme
export FZF_DEFAULT_OPTS=" \
--color=bg+:#414559,bg:#303446,spinner:#f2d5cf,hl:#e78284 \
--color=fg:#c6d0f5,header:#e78284,info:#ca9ee6,pointer:#f2d5cf \
--color=marker:#f2d5cf,fg+:#c6d0f5,prompt:#ca9ee6,hl+:#e78284"

######
# Aliases
###

# eza & ls
FINDBIN=""
if [ "$(command -v eza)" ]; then
    FINDBIN="eza"
elif [ "$(command -v exa)" ]; then
    FINDBIN="exa"
fi
alias ls="$FINDBIN -G  --color always -a -s type"
alias ll="$FINDBIN -l --color always -aa -g -h -s type --icons"
alias la="$FINDBIN -l --color always -aa -g -h -H -i -s type --icons"
alias l="$FINDBIN -l --color always -h -s type --icons"
alias lt="$FINDBIN -l --color always -a -g -h -T -L2 -s type --icons"
alias ltt="$FINDBIN -l --color always -a -g -h -T -L3 -s type --icons"
alias lr="$FINDBIN -l --color always -a -g -h -r -s size --icons"

# fzf
alias ff="fzf --height 80% --preview '$BATCMD --style=numbers --color=always --line-range :500 {}'"

# git
alias gaa='git add --all'
alias gcam='git commit --all --message'
alias gd='git diff'
alias gl='git pull'
alias gp='git push'
alias gst='git status'
alias gss='git status --short'

# trash
alias rm='trash'

# dailys
alias -g grep='grep --color=auto'
alias x='exit '
alias :q='exit '

######
# Functions
###

# print file without empty or commented lines
confgrep() {
    grep -v ^$ $1 | grep -v ^\# | cat
}

# pint short info about given process
proc() {
    ps -ef | grep $1 | cat
}

# update
upup() {
    if [ "$(command -v rpm)" ]; then
        sudo dnf upgrade -y --refresh
    elif [ "$(command -v dpkg)" ]; then
        sudo apt update -y && sudo apt upgrade -y
    else
        echo "Warn: Not a rpm or dpkg based system. Aborting"
    fi

    if [ "$(command -v flatpak)" ]; then
        flatpak update -y
    fi
}

######
# Keybinds
###

#bindkey '^[[A' history-substring-search-up
#bindkey '^[[B' history-substring-search-down

######
# Autocompletion
###

zstyle :compinstall filename "$HOME/.zshrc"
autoload -Uz compinit
compinit

######
# Prompt
###

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
