#!/bin/bash

# function for usage errors
usage() { echo "Usage: $0 [-z] [-e] [-t]"; }

# check if options are empty
#[ $# -eq 0 ] && echo "$0: Options cannot be empty!" && usage ; exit 1;

# get options
while getopts ':zethu' OPTION; do

    # do actions for every option
    case "$OPTION" in
        z)
            # zsh actions
            ZSHDIR="$HOME/.zsh"
            [ ! -d "$ZSHDIR" ] && mkdir -p "$ZSHDIR"

            # install antigen
            ANTIDIR="$ZSHDIR/antigen"
            if [ ! -d "$ANTIDIR" ]; then
                echo "Installing antigen."
                git clone https://github.com/zsh-users/antigen.git "$ANTIDIR"
            else
                echo "Antigen is already installed in $ANTIDIR. Skipping"
            fi

            # install pure prompt
            PUREDIR="$ZSHDIR/pure"
            if [ ! -d "$PUREDIR" ]; then
                echo "Installing pure prompt."
                git clone https://github.com/sindresorhus/pure.git "$PUREDIR"
            else
                echo "Updating pure prompt."
                cd "$PUREDIR" && git pull --no-rebase
            fi

            ;;
        e)
            # install exa
            echo "Installing exa."
            EXA_V=$(curl -s "https://api.github.com/repos/ogham/exa/releases/latest" | grep -Po '"tag_name": "v\K[0-9.]+')
            curl -Lo exa.zip "https://github.com/ogham/exa/releases/latest/download/exa-linux-x86_64-v${EXA_V}.zip"
            sudo unzip -q exa.zip bin/exa -d /usr/local
            rm exa.zip

            ;;
        t)
            # install tmux-plugin-manager
            TPMDIR="$HOME/.tmux/plugins/tpm"
            if [ ! -d "$TPMDIR" ]; then
                echo "Installing tpm."
                git clone https://github.com/tmux-plugins/tpm "$TPMDIR"
            else
                echo "Updating tpm."
                cd ~/.tmux/plugins/tpm && git pull --no-rebase
            fi

            ;;
        h)
            # help and usage prompt
            usage
            echo "Install script for several user modules and plugins."
            echo
            echo "Options:"
            echo "-h    Print this help prompt"
            echo "-z    Install zsh modules"
            echo "-e    Install exa"
            echo "-t    Install tmux-plugin-manager"

            exit 1 ;;
        ?|*)
            # print error for invalid option
            echo "$0: No valid option -- $OPTARG"
            usage

            exit 1 ;;
    esac

done
shift $((OPTIND-1))
