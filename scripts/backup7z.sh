#!/bin/bash

###
# global variables
###

HOMEDIR="$HOME"
BCKDIR="$HOMEDIR/Backup/Archives" # set archive destination
DATE="$(date -I)"
PWCHECK="12" # enter minimum password length
BCKNAME=""
BCKPW=""
BCKPW1=""
BCKPW2=""

###
# styling
###

red=$(tput setaf 1)
blue=$(tput setaf 4)
bold=$(tput bold)
normal=$(tput sgr0)

###
# prechecks
###

if [ ! "$(command -v 7z)" ]; then
    echo -e "${red}${bold}WARNING${normal}: 7z not found!\nTry installing p7zip and p7zip-plugins."
    exit 1
fi

###
# prompt for password
###

echo -e "\n${blue}7zBackup${normal}\n\nEnter Encryption Passwords"
read -s -r -p "Password: " BCKPW1
echo
read -s -r -p "Retype Password: " BCKPW2

if [ "$BCKPW1" != "$BCKPW2" ]; then
    echo -e "\n\n${red}${bold}ERROR: Passwords don't match!${normal}"
    exit 1
elif [ ! ${#BCKPW1} -ge "$PWCHECK" ]; then
    echo -e "\n\n${red}${bold}ERROR: Password too short!${normal}"
    exit 1
else
    BCKPW=$BCKPW1
    BCKPW1=""
    BCKPW2=""
    echo -e "\n"
fi

###
# functions
###

bckerr () {
    local BCKSTATUS=$?
    local BCKFOLD=$1

    if [ "$BCKSTATUS" != 0 ]; then
        echo -e "\n${red}${bold}ERROR${normal}: Backing up ${bold}\"$BCKFOLD\"${normal} failed! (${red}$BCKSTATUS${normal})\n"
        #[ $BCKFOLD ] && rm "$BCKDIR/$BCKFOLD.7z"
        #exit $BCKSTATUS
    fi
}

bckexists () {
    local EXISTNAME=$1
    echo -e "${blue}INFO${normal}: ${bold}$EXISTNAME.7z${normal} exists"
}

###
# start backup process
###

for bckfolder in $BCKDIR $BCKDIR/Documents
do
    if [ ! -d "$bckfolder" ]; then
        echo "${blue}INFO${normal}: Creating $bckfolder"
        mkdir -p "$bckfolder"
    fi
done

BCKNAME="Pictures"
if [ ! -e "$BCKDIR/$BCKNAME.7z" ]; then
    /usr/bin/7z a "$BCKDIR/$BCKNAME.7z" "$HOMEDIR/Pictures" -p"$BCKPW" -mhe=on -mx=0
    bckerr "$BCKNAME"
else
    bckexists "$BCKNAME"
fi

BCKNAME="Documents_$DATE"
if [ ! -e "$BCKDIR/Documents/$BCKNAME.7z" ]; then
    /usr/bin/7z a "$BCKDIR/Documents/$BCKNAME.7z" "$HOMEDIR/Documents" -p"$BCKPW" -mhe=on
    bckerr "$BCKNAME"
else
    bckexists "$BCKNAME"
fi

echo -e "\nBackups have been created in ${bold}$BCKDIR${normal}"

###
# clean up
###

BCKPW=""

exit 0
