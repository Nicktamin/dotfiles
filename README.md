# Nick's dotfiles
All the little dots connected form a line so powerfull that they can withstand every scenario.

## Apps
- lutris
- prism launcher
- steam
- protonplus
- brave
- discord
- librewolf
- motrix
- mullvad vpn
- qbittorrent
- signal
- syncthing
- mpv
- vlc
- onlyoffice
- bleachbit
- gparted
- timeshift
- pika backup
- bitwarden
- flatseal
- keepassxc
- localsend
- piper
- solaar
- libation
- Czkawka
- libheif

## Terminal
- zsh
- vim
- shellcheck
- eza (/exa)
- tmux
- fzf
- ranger
- fd-find
- bat (/batcat)
- git
- trash-cli
- tldr
- btop
- htop
- nvtop
- ncdu
- visidata
- vkbasalt
- gamemode
- p7zip
- prename

## Styling
### Appearance
- Cursor:         Bibata-Modern-Ice
- Icons:          Tela
- Shell:          Catppucin Frappe Standard Sapphire Dark
- Applications:   Catppucin Frappe Standard Sapphire Dark
- Colorscheme:    Catppucin

### Fonts
- Monospace:      FiraCode Nerd Font Mono Regular, 12
